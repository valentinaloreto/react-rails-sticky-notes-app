class Note extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      editable: this.props.notesDefaultState,
      formFields: {}
    }
    this.changeEditable = this.changeEditable.bind(this)
  }

  componentWillReceiveProps(nextProps){
    // alert(nextProps.value);
    this.setState({editable:nextProps.value});
  }

  changeEditable() { this.setState({ editable: !this.state.editable }) }

  render(){
    if (this.state.editable) {
      contingent_content =
        <form onSubmit={ (e) => {  this.props.handleUpdate(this.props.note.id, this.content.value); e.stopPropagation(); } } >
          <textarea
            className='editable-txarea'
            id="valueedit"
            ref={input => this.content = input}
            defaultValue={this.props.note.content}
          />
          <button className='button update-button '>Update</button>
        </form>
    } else {
      contingent_content =  <p>{ this.props.note.content }</p>
    }


    return(
      <div onDoubleClick={   this.changeEditable   } onClick={ (e) => {   e.stopPropagation(); }  }>
        {contingent_content}
      </div>
    )
  }
}
