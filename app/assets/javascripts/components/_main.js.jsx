class Main extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      notes: [],
      notesDefaultState: false
    };
     // **************************************************
    this.handleNewNoteFormSubmit = this.handleNewNoteFormSubmit.bind(this)
    this.addNewNote = this.addNewNote.bind(this)
    // **************************************************
    this.handleDelete = this.handleDelete.bind(this)
    this.deleteNote = this.deleteNote.bind(this)
    // **************************************************
    this.hideNewNoteForm = this.hideNewNoteForm.bind(this)
     // **************************************************
    this.handleUpdate = this.handleUpdate.bind(this);

    this.clickOutsideNote = this.clickOutsideNote.bind(this)
  }

  componentDidMount(){
    fetch('/api/v1/notes.json')
      .then((response) => {return response.json()})
      .then((data) => {this.setState({ notes: data }) });
  }

  // **************************************************

  handleNewNoteFormSubmit(content){
    console.log('inside method');
    let body = JSON.stringify({note: {content: content} })
    fetch('http://localhost:3000/api/v1/notes', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: body,
      }).then((response) => {return response.json()})
      .then((note)=>{
        this.addNewNote(note)
      })
  }

  addNewNote(note){
    this.setState({
      notes: this.state.notes.concat(note)
    })
  }

// **************************************************

  handleDelete(id){
    fetch(`http://localhost:3000/api/v1/notes/${id}`,
    {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((response) => {
      this.deleteNote(id)
      })
  }

  deleteNote(id){
    newNotes = this.state.notes.filter((note) => note.id !== id)
    this.setState({
      notes: newNotes
    })
  }

  // **************************************************

  revealNewNoteForm() {
    new_note_form = document.querySelector('#new-note-form');
    new_note_form.style.display = 'block'
  }

  hideNewNoteForm() {
    new_note_form = document.querySelector('#new-note-form');
    new_note_form.style.display = 'none'
  }



  // **************************************************

  handleUpdate(id, content){

    fetch(`http://localhost:3000/api/v1/notes/${id}`,
    {
      method: 'PUT',
      body: JSON.stringify({note: {content: content} }),
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((response) => {

    })
  }

  // **************************************************

  clickOutsideNote(e) {
    all_notes = document.querySelectorAll(".note-container");
    ar = Array.from(all_notes);
    if ( !ar.includes(e.target) ) {
      this.setState({
        notesDefaultState: false
      })
    }
  }

  // **************************************************

  render () {
    return(
      <div className='border-gree' onClick={ (e) => { this.clickOutsideNote(e); } } >
        <AllNotes handleUpdate={ this.handleUpdate } notes={ this.state.notes } handleDelete={ this.handleDelete }
        notesDefaultState={ this.state.notesDefaultState }/>
        <button className='buttons new-note-button'
          onClick={ (e) => {this.revealNewNoteForm(); e.stopPropagation(); }  }
          >New Note</button>
        <NewNote hideNewNoteForm={ this.hideNewNoteForm } handleNewNoteFormSubmit={ this.handleNewNoteFormSubmit } />
      </div>
    )
  }
}
