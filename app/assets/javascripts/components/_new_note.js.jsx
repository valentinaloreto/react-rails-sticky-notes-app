const NewNote = (props) => {
  let formFields = {}

  return(
    <div className=" fixed-div" id="new-note-form" onClick={ (e) => { props.hideNewNoteForm(); e.stopPropagation(); } } >
      <form className="relative-form" onSubmit={ (e) => { props.handleNewNoteFormSubmit(formFields.content.value); e.target.reset();} }>
        <textarea
        className='form-input-fields'
        ref={input => formFields.content = input}
        onClick={ (e) => { e.stopPropagation(); } }
        placeholder='Write note content here...'
        />
        <button className='buttons absolute-submit'>Save</button>
      </form>
    </div>
  )
}
