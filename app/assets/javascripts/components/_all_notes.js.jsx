const AllNotes = (props) => {


    var notes = props.notes.map ( (note)=> {
      return(
        <div className='border-re note-container' key={note.id} id={"note-id-" + note.id} onClick={(e)=>{e.stopPropagation();}}>
          <span onClick={ () => props.handleDelete(note.id) } className="x-delete">X</span>
          <Note handleUpdate={ props.handleUpdate } note={note} notesDefaultState={ props.notesDefaultState }/>
        </div>
      )
    })
    return(
      <div className='border-blu main-container'>
        {notes}
      </div>
      )

  }
